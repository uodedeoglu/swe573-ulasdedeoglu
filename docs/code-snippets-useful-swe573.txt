Set-ExecutionPolicy Unrestricted -Scope Process
scripts/activate
deactivate
psycopg2>=2.8.6,<2.8.7

&& apt-get -y install libpq-dev gcc \

RUN apt-get update \
    && apt-get -y install libpq-dev gcc \
    && pip install psycopg2

UPDATE articles_article SET search_vector = 
	setweight(to_tsvector(coalesce(title,'')), 'A') ||
	setweight(to_tsvector(coalesce(keywords,'')), 'B') ||
	setweight(to_tsvector(coalesce(abstract,'')), 'B');

def term_search(request, term):
    search_term = term
    if not search_term:
        raise Http404('Send a search term')

    articles = search(search_term)

    response_data = [
        {
            'rank': art.rank,
            'title': art.title,
            'url': "articles/" + str(art.id) + "\n",
        } for art in articles
    ]

    return HttpResponse(
        json.dumps(response_data),
        content_type='application/json',
    )
	
	
def search(self, search_text):
    search_vectors = (
            SearchVector('title', weight='A')
            + SearchVector('keywords', weight='B')
            + SearchVector('abstract', weight='C')
    )
    search_query = SearchQuery(
        search_text, config='english', search_type='phrase'
    )

    search_rank = SearchRank(search_vectors, search_query)

    qs = (
        self.get_queryset()
            .filter(search_vector=search_query)
            .annotate(rank=search_rank)
            .order_by('-rank')
    )

    if qs or qs is not []:
        return qs
    else:
        return self.filter_by_trigram_similarity(qs, 'title', search_text, similarity=0.1)


def filter_by_trigram_similarity(self, qs, field_name, search_term, similarity=0.15):
    if not search_term:
        return qs

    similarity_name = field_name + '_similarity'
    strict_matching_name = field_name + '_strict_matching'

    qs = qs.annotate(
        **{
            strict_matching_name: Case(
                When(**{field_name + '__iexact': search_term, 'then': 3}),
                When(**{field_name + '__istartswith': search_term, 'then': 2}),
                When(**{field_name + '__icontains': search_term, 'then': 1}),
                output_field=IntegerField(),
                default=0
            )
        }
    )

    qs = qs.annotate(**{similarity_name: TrigramSimilarity(field_name, search_term)})

    qs = qs.filter(
        Q(**{similarity_name + '__gt': similarity}) |
        # following line is required if column's max length is big,
        # because in this case similarity can be less
        # than minimum similarity, but strict match exists
        Q(**{strict_matching_name + '__gt': 0})
    )

    return qs.order_by(
        '-' + strict_matching_name,
        '-' + similarity_name)