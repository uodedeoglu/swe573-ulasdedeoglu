from Bio import Entrez
from Bio import Medline

from datetime import datetime

Entrez.email = "ulas.dedeoglu@boun.edu.tr"
Entrez.api_key = "8e01a377f3dcb7b4d90a83887c16a109b508"

initial_db_docs = []
start = 0

handle = Entrez.esearch(db="pubmed", term="sports", retstart=start, retmax=100)
record = Entrez.read(handle)
doc_ids = record['IdList']

handle = Entrez.efetch(db="pubmed", rettype="Medline", id=doc_ids, retmode="text")
record = Medline.parse(handle)

for row in record:
    title = ""
    doc_abs = ""
    authors = ""
    date = ""
    doi = ""
    lan = ""
    keywords = ""
    pub_date = None

    try:
        title = row['TI']
        doc_abs = row['AB']
        authors = ' - '.join(row['FAU'])
        doi = ""
        keywords = row['OT']

        for i in row['AID']:
            if "[doi]" in i:
                doi = i[0:i.find("[doi]") - 2]

        lan = row['LA']

        try:
            pub_date = datetime.strptime(row['DP'], "%Y %b %d")
        except ValueError:
            try:
                pub_date = datetime.strptime(row['DP'], "%Y %b-%d")
            except ValueError:
                try:
                    pub_date = datetime.strptime(row['DP'], "%Y %b")
                except ValueError:
                    pub_date = datetime.strptime(row['DP'], "%Y")
    except (KeyError, NameError, TypeError, ValueError):
        continue

    finally:
        if title != "" and doc_abs != "" and authors != "" and \
                pub_date != "" and doi != "" and str(lan) == "['eng']" and pub_date is not None:
            print(pub_date)

