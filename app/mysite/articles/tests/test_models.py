from django.test import TestCase

from ..models import Article

import datetime


class ArticleTests(TestCase):
    def setUp(self):
        temp_article = Article()
        temp_article.title = "Trial article"
        temp_article.abstract = "Lorem ipsum"
        temp_article.authors = "Ulas Dedeoglu"
        temp_article.pub_date = datetime.datetime.now()
        temp_article.doi = "10.2311/244"
        temp_article.keywords = "trial, testing, temp"
        temp_article.save()

    def test_fields(self):
        record = Article.objects.get(title="Trial article")

        self.assertTrue(record is not None)
        self.assertEqual(record.title, "Trial article")
        self.assertEqual(record.abstract, "Lorem ipsum")
        self.assertEqual(record.authors, "Ulas Dedeoglu")
        self.assertIsInstance(record.pub_date, datetime.date)
        self.assertEqual(record.doi, "10.2311/244")
        self.assertFalse(record.keywords == "")

    def test_create(self):
        self.assertRaises(Article.DoesNotExist, Article.objects.get, pk=2)

        temp_article = Article(title="Trial 2", pub_date=datetime.datetime.now())
        temp_article.save()

        article_2 = Article.objects.get(pk=2)
        self.assertTrue(article_2)
        self.assertEqual(article_2.title, "Trial 2")
        self.assertEqual(article_2.abstract, "")
