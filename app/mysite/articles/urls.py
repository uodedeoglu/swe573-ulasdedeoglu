from django.urls import path

from . import views


urlpatterns = [
    path('', views.latest_articles, name='latest_articles'),
    path('log_out', views.log_out, name='log_out'),
    path('search_page', views.search_page, name='search_page'),
    path('results', views.results, name="results"),
    path('detail/<int:doc_id>', views.article_detail, name="article_detail"),
]
