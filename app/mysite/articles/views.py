from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect

import django.core.exceptions

from .models import fetch_latest, Article, ArticleTag, Tag
from .forms import CreateTagForm

from django.contrib.auth import logout

import json


def latest_articles(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(f'http://{request.get_host()}')
    else:
        return render(request, 'latest.html', {'latest_articles': fetch_latest(),
                                               'c_nav_links': {'1': {'url': 'search_page', 'text': 'Search'},
                                                               '2': {'url': 'log_out', 'text': 'Logout'}},
                                               'host': request.get_host()})


def log_out(request):
    logout(request)
    return HttpResponseRedirect(f'http://{request.get_host()}')


def search_page(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(f'http://{request.get_host()}')
    else:
        return render(request, 'search.html', {'c_nav_links': {'1': {'url': f'http://{request.get_host()}/articles',
                                                                     'text': 'Latest'},
                                                               '2': {'url': 'log_out', 'text': 'Logout'}},
                                               'host': request.get_host()})


def results(request):
    term = request.POST.get('search_term', None)

    articles = Article.objects.search(term)

    response_data = {}

    for index, art in enumerate(articles):
        response_data[index] = {'rank': art.rank,
                                'title': art.title,
                                'url': f"http://{request.get_host()}/articles/detail/" + str(art.id) + "\n"}

    return render(request, 'results.html', {'response_data': response_data,
                                            'c_nav_links': {
                                                '1': {'url': f'http://{request.get_host()}/articles/search_page',
                                                      'text': 'Search'},
                                                '2': {'url': f'http://{request.get_host()}/articles',
                                                      'text': 'Latest'},
                                                '3': {'url': f'http://{request.get_host()}/articles/log_out',
                                                      'text': 'Logout'}
                                            },
                                            'host': request.get_host()})


def article_detail(request, doc_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(f'http://{request.get_host()}')
    else:
        if request.method == "POST":
            if 'create_tag_form' in request.POST:
                create_tag_form = CreateTagForm(request.POST)
                if create_tag_form.is_valid():
                    create_tag_form.save()
                    tag_to_add = ArticleTag.objects.create(article=Article.objects.get(pk=doc_id))
                    tag_to_add.tags.add(Tag.objects.last())
                    tag_to_add.save()
                    return HttpResponseRedirect(f'http://{request.get_host()}'+request.path_info)
            if 'add_tag_form' in request.POST:
                add_tag_field = request.POST.get('add_tag_field', None)
                if add_tag_field is not None:
                    tag = Tag.objects.get(title=add_tag_field)
                    if tag is not None:
                        tag_to_add = ArticleTag.objects.create(article=Article.objects.get(pk=doc_id))
                        tag_to_add.tags.add(tag)
                        tag_to_add.save()
                return HttpResponseRedirect(f'http://{request.get_host()}'+request.path_info)

        else:
            article = None
            tag_list = []

            try:
                article = Article.objects.get(pk=doc_id)
            except DoesNotExist:
                pass
            finally:
                try:
                    article_tags = ArticleTag.objects.filter(article=article)
                    for article_tag in article_tags:
                        print(article_tag)
                        tag_list.append(article_tag.tags.all())
                except DoesNotExist:
                    pass
                finally:
                    create_tag_form = CreateTagForm()
                    return render(request, 'detail.html', {'article': article,
                                                           'tag_list': tag_list,
                                                           'create_tag_form': create_tag_form,
                                                           'c_nav_links': {'1': {
                                                               'url': f'http://{request.get_host()}/articles/search_page',
                                                               'text': 'Search'},
                                                               '2': {
                                                                   'url': f'http://{request.get_host()}/articles',
                                                                   'text': 'Latest'},
                                                               '3': {
                                                                   'url': f'http://{request.get_host()}/articles/log_out',
                                                                   'text': 'Logout'}},
                                                           'host': request.get_host()})
