from Bio import Entrez
from Bio import Medline

from datetime import datetime

from django.db import models, migrations

import sys

def load_articles(apps, schema_editor):
    Entrez.email = "ulas.dedeoglu@boun.edu.tr"
    Entrez.api_key = "8e01a377f3dcb7b4d90a83887c16a109b508"

    initial_db_docs = []
    start = 0

    handle = Entrez.esearch(db="pubmed", term="sports", retstart=start, retmax=5000)
    record = Entrez.read(handle)
    count = int(record['Count'])

    while len(initial_db_docs) < 45000 and start < count:
        handle = Entrez.esearch(db="pubmed", term="sports", retstart=start, retmax=5000)
        record = Entrez.read(handle)

        doc_ids = record['IdList']

        handle = Entrez.efetch(db="pubmed", rettype="Medline", id=doc_ids, retmode="text")
        record = Medline.parse(handle)

        for row in record:
            title = ""
            doc_abs = ""
            authors = ""
            date = ""
            doi = ""
            lan = ""
            keywords = ""
            pub_date = None

            try:
                title = row['TI']
                doc_abs = row['AB']
                authors = ' - '.join(row['FAU'])
                date = row['DP']
                doi = ""
                keywords = row['OT']

                for i in row['AID']:
                    if "[doi]" in i:
                        doi = i[0:i.find("[doi]") - 2]

                lan = row['LA']

                try:
                    pub_date = datetime.strptime(row['DP'], "%Y %b %d")
                except ValueError:
                    try:
                        pub_date = datetime.strptime(row['DP'], "%Y %b-%d")
                    except ValueError:
                        try:
                            pub_date = datetime.strptime(row['DP'], "%Y %b")
                        except ValueError:
                            pub_date = datetime.strptime(row['DP'], "%Y")

            except (KeyError, ValueError, NameError, TypeError):
                continue

            finally:
                if title != "" and doc_abs != "" and authors != "" and \
                        pub_date != "" and doi != "" and str(lan) == "['eng']" and pub_date is not None:
                    initial_db_docs.append((title, doc_abs, authors, pub_date, doi, keywords))

        start += 5000
        print(len(initial_db_docs))

    for doc in initial_db_docs:
        Article = apps.get_model("articles", "Article")
        temp_article = Article(title=doc[0], abstract=doc[1], authors=doc[2],
                               pub_date=doc[3].date(), doi=doc[4], keywords=doc[5])
        temp_article.save()


class Migration(migrations.Migration):
    dependencies = [
        ('articles', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_articles)
    ] if 'test' not in sys.argv[1:] else []
