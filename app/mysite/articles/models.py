from django.db import models
from django.contrib.postgres.search import SearchVector, SearchVectorField, SearchQuery, SearchRank, TrigramSimilarity
from django.db.models import Case, When, Q, IntegerField
from django.contrib.postgres.aggregates import StringAgg


class ArticleManager(models.Manager):

    def search(self, search_text):
        search_vectors = (
                SearchVector(
                    'title', weight='A', config='english'
                )
                + SearchVector('keywords', weight='B')
                + SearchVector('abstract', weight='B')
        )
        search_query = SearchQuery(
            search_text, config='english'
        )
        search_rank = SearchRank(search_vectors, search_query)
        qs = (
            self.get_queryset()
                .filter(search_vector=search_query)
                .annotate(rank=search_rank)
                .order_by('-rank')
        )
        return qs


class Article(models.Model):
    title = models.TextField()
    abstract = models.TextField()
    authors = models.TextField()
    pub_date = models.DateField()
    doi = models.TextField()
    keywords = models.TextField()
    search_vector = SearchVectorField(null=True)

    objects = ArticleManager()


def fetch_latest():
    articles = Article.objects.filter().order_by('-pub_date')[:20]
    latest_articles = {}
    for index, article in enumerate(articles):
        title = article.title
        pub_date = article.pub_date
        authors = article.authors
        abstract = article.abstract

        latest_articles[index] = {'title': title,
                                  'pub_date': pub_date,
                                  'authors': authors,
                                  'abstract': abstract,
                                  'id': article.pk}
    return latest_articles


class Tag(models.Model):
    wikidata_url = models.TextField()
    description = models.TextField()
    title = models.TextField()


class ArticleTag(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag, related_name='tags')
