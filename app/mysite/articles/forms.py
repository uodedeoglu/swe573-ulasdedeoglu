from django import forms
from django.forms import ModelForm
from .models import Tag


class CreateTagForm(ModelForm):
    class Meta:
        model = Tag
        fields = "__all__"
        widgets = {
            'wikidata_url': forms.TextInput(
                attrs={'placeholder': 'Wikidata URL'}),
            'description': forms.Textarea(
                attrs={'placeholder': 'Description of the tag'}),
            'title': forms.TextInput(
                attrs={'placeholder': 'Tag Title'})
        }
        labels = {
            "wikidata_url": "",
            "description": "",
            "title": ""
        }
