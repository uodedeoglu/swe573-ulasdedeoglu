from django.urls import path

from . import views

urlpatterns = [
    path('', views.sign_up, name='signup'),
    path('', views.sign_in, name='signin'),
]