from django.contrib import admin
from .models import SignUpRequest, Admin, User

admin.site.register(SignUpRequest)
admin.site.register(Admin)
admin.site.register(User)
