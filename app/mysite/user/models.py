from django.db import models
from django.db import connection


class SignUpRequest(models.Model):
    mail = models.EmailField()
    password = models.CharField(max_length=16)
    organization = models.CharField(max_length=50)
    purpose = models.TextField()


class User(models.Model):
    mail = models.EmailField()
    password = models.CharField(max_length=16)
    is_private_profile = models.BooleanField()


class Admin(models.Model):
    mail = models.EmailField()
    password = models.CharField(max_length=16)
    last_activity = models.DateTimeField()
