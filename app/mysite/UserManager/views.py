from django.contrib.auth import login

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import SignUpRequestForm, SignInForm
from django.contrib.auth.forms import AuthenticationForm


def index(request):
    if request.user.is_authenticated:
        return redirect('/articles/')

    sign_up_request_submitted = False
    error_sign_up_request = False
    error_sign_in_request = False

    if request.method == 'POST':
        if 'sign_up_form' in request.POST:
            sign_up_form = SignUpRequestForm(request.POST)
            if sign_up_form.is_valid():
                sign_up_form.save()
                return HttpResponseRedirect('?submitted=True')
            else:
                return HttpResponseRedirect('?su_error=True')
        if 'sign_in_form' in request.POST:
            sign_in_form = AuthenticationForm(data=request.POST)
            if sign_in_form.is_valid():
                user = sign_in_form.get_user()
                login(request, user)
                redirect('/articles/')
        return HttpResponseRedirect('?si_error=True')
    else:
        sign_up_form = SignUpRequestForm()
        sign_in_form = SignInForm()
        if 'submitted' in request.GET:
            sign_up_request_submitted = True
            return render(request, 'homepage.html', {'sign_up_form': sign_up_form,
                                                     'sign_in_form': sign_in_form,
                                                     'sign_up_error': error_sign_up_request,
                                                     'sign_in_error': error_sign_in_request,
                                                     'submitted': sign_up_request_submitted,
                                                     'host': request.get_host()})
        elif 'su_error' in request.GET:
            error_sign_up_request = True
            return render(request, 'homepage.html', {'sign_up_form': sign_up_form,
                                                     'sign_in_form': sign_in_form,
                                                     'sign_up_error': error_sign_up_request,
                                                     'sign_in_error': error_sign_in_request,
                                                     'submitted': sign_up_request_submitted,
                                                     'host': request.get_host()})
        elif 'si_error' in request.GET:
            error_sign_in_request = True
            return render(request, 'homepage.html', {'sign_up_form': sign_up_form,
                                                     'sign_in_form': sign_in_form,
                                                     'sign_up_error': error_sign_up_request,
                                                     'sign_in_error': error_sign_in_request,
                                                     'submitted': sign_up_request_submitted,
                                                     'host': request.get_host()})
        return render(request, 'homepage.html', {'sign_up_form': sign_up_form,
                                                 'sign_in_form': sign_in_form,
                                                 'sign_up_error': error_sign_up_request,
                                                 'sign_in_error': error_sign_in_request,
                                                 'submitted': sign_up_request_submitted,
                                                 'host': request.get_host()})
