from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from .models import SignUpRequest


class SignUpRequestForm(ModelForm):
    class Meta:
        model = SignUpRequest
        fields = "__all__"
        widgets = {
            'user_name': forms.TextInput(
                attrs={'placeholder': 'Username'}),
            'email': forms.EmailInput(
                attrs={'placeholder': 'ex: name@company.com'}),
            'name': forms.TextInput(
                attrs={'placeholder': 'ex: JohnDoe'}),
            'password': forms.PasswordInput(attrs={'placeholder': 'password', 'autocomplete': 'on'}),
            'sop': forms.Textarea(
                attrs={'placeholder': 'Brief statement of purpose'})
        }
        labels = {
            "user_name": "",
            "email": "",
            "name": "",
            "password": "",
            "sop": "",
        }


class SignInForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password']

        widgets = {
            'username': forms.TextInput(
                attrs={'placeholder': 'ex: username'}),
            'password': forms.PasswordInput(
                attrs={'placeholder': 'password', 'autocomplete': 'on'}),
        }

        labels = {
            "username": "",
            "password": "",
        }

        help_texts = {
            'username': "",
            'password': ""
        }
