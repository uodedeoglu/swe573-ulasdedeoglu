from django.db import models


class SignUpRequest(models.Model):
    user_name = models.CharField(null=None, primary_key=True, unique=True, max_length=20)
    email = models.EmailField(null=None, unique=True)
    name = models.CharField(max_length=50, null=True)
    password = models.CharField(max_length=16)
    sop = models.TextField()