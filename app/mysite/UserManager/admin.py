from django.contrib import admin
from .models import SignUpRequest

admin.site.register(SignUpRequest)